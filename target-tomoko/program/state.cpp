auto Program::stateName(uint slot, bool manager) -> string {
  return {
    folderPaths[0], "higan/states/",
    manager ? "managed/" : "quick/",
    "slot-", natural(slot, 2L), ".bst"
  };
}

auto Program::loadState(uint slot, bool manager) -> bool {
  if(!emulator) return false;
  auto location = stateName(slot, manager);
  auto memory = file::read(location);
  if(memory.size() == 0) return showMessage({"Slot ", slot, " does not exist"}), false;
  serializer s(memory.data(), memory.size());
  if(emulator->unserialize(s) == false) return showMessage({"Slot ", slot, " state incompatible"}), false;
  return showMessage({"Loaded from slot ", slot}), true;
}

auto Program::saveState(uint slot, bool manager) -> bool {
  if(!emulator) return false;
  auto location = stateName(slot, manager);
  serializer s = emulator->serialize();
  if(s.size() == 0) return showMessage({"Failed to save state to slot ", slot}), false;
  directory::create(pathname(location));
  if(file::write(location, s.data(), s.size()) == false) {
    return showMessage({"Unable to write to slot ", slot}), false;
  }
  return showMessage({"Saved to slot ", slot}), true;
}

auto Program::incrementState() -> void {
  if(activeSaveState < MAX_SAVESTATE_SLOT) {
    activeSaveState++;
  }
  return showMessage({"Switched to save slot ", activeSaveState});
}
auto Program::decrementState() -> void {
  if(activeSaveState > 0) {
    activeSaveState--;
  }
  return showMessage({"Switched to save slot ", activeSaveState});
}

auto Program::activeState() const -> uint {
  return activeSaveState;
}
